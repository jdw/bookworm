import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getBookForm() {
    return element(by.css('app-book'));
  }

  getTitleTextbox() {
    return element(by.name('title'));
  }

  getSubmitButton() {
    return element(by.name('submit-button'));
  }

  getBookList() {
    return element(by.css('app-book-list')).all((by.css('.book-details')));
  }

  selectOption() {
    return element.all(by.tagName('option'))
      .then(function(options) {
        options[0].click();
      });
  }

  getCategorySelector() {
    return element(by.name('category'));
  }

  getDescriptionTextbox() {
    return element(by.name('description'));
  }
}
