import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to bookworm!');
  });

  it('should display the add book form', () => {
    page.navigateTo();
    expect(page.getBookForm()).toBeTruthy();
  });

  it('can add a book to the list', () => {
    page.getTitleTextbox().sendKeys('my book');
    page.getDescriptionTextbox().sendKeys('A fantastic read about stuff');
    page.selectOption();
    page.getSubmitButton().click();
    const list = page.getBookList();
    expect(list.isPresent()).toBeTruthy();
  });
});
