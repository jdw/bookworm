import { Component } from '@angular/core';
import { BookService } from './books/book.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'bookworm';
  constructor(public service: BookService) { }
}
