export class Book {
  constructor(
    public title: string,
    public category: 'drama' | 'comedy' | 'sport',
    public description: string
  ) { }
}
