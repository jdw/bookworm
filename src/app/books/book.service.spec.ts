import { TestBed } from '@angular/core/testing';

import { BookService } from './book.service';
import { Book } from './../book';

describe('BookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookService = TestBed.get(BookService);
    expect(service).toBeTruthy();
  });

  it('can store some books', () => {
    const service: BookService = TestBed.get(BookService);

    service.addBook(new Book('foo', 'comedy', 'test book'));
    expect(service.getBooks().length).toEqual(1);

    service.addBook(new Book('bar', 'drama', 'test book'));
    expect(service.getBooks().length).toEqual(2);
  });
});
