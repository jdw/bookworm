import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookService } from './../book.service';
import { Book } from './../../book';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent {
  private angularForm: FormGroup;

  public categories = [
    'comedy',
    'drama',
    'sport'
  ];

  constructor(private fb: FormBuilder, private service: BookService) { // <--- inject FormBuilder
    this.createForm();
  }

  createForm() {
    this.angularForm = this.fb.group({
      title: ['', [Validators.required, Validators.maxLength(30)]],
      category: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.angularForm.invalid) {
      return;
    }
    const { title, category, description } = this.angularForm.value;

    this.service.addBook(new Book(title, category, description));
    this.angularForm.reset();
  }
}
