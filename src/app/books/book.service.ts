import { Injectable } from '@angular/core';
import { Book } from './../book';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  private books: Book[];

  constructor() {
    this.books = [];
  }

  getBooks() {
    return this.books;
  }

  addBook(book: Book) {
    this.books.push(book);

    return this;
  }
}
