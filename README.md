# Bookworm

Bookworm is a toy application for reading aficionados which allows users to
create there own book lists. Simply enter a books details and save it for later!

#### Features

- [x] Create a reading lists and persist in memory
- [x] Fully tested
- [x] Automated CI [pipeline](https://gitlab.com/jdw/bookworm/pipelines)

#### Architecture

Bookworm is build using [Angular 6]() and functions as a Single Page Application
(SPA). It is stateless and saves its data in-memory. Be warned if you refresh
the page you will lose your data :)

## Getting Started

To get up and running with bookworm on your computer begin by cloning this
repository.

```shell
git clone git@gitlab.com:jdw/bookworm.git
cd bookworm
```

Next install all development dependencies;

```shell
npm i
```

Now boot up the development server with `npm start` and navigate to
`http://localhost:4200` to view the app.

Below we outline some other useful commands to get you started.

##### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

##### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

##### Build

Run `ng build` to build the project. The build artefacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

##### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

##### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
